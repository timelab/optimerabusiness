<?php
/*
Template Name: Undersida Mall
*/
$hasHead = false;
?>

<div class="wrap container mainText PodsBrands subpage" role="document">
  <div class="row relative">
      <div class="col-xs-12 subHeader">
        <?php get_template_part('templates/page', 'header'); ?>
      </div>
    </div>
    
    <div class="row relative minPageHeight">
      <div class="col-md-12 subContent">
      <?php get_template_part('templates/content', 'page'); ?>
      <?php if(get_post_meta( get_the_id(), "contact_title", true ) != null){
              $hasHead = true;
              $headText = get_post_meta( get_the_id(), "contact_title", true );
              echo "<hr />";
              echo "<h2>" . $headText . "</h2>";
            }

            if(get_post_meta( get_the_id(), "contact_id", true ) != null){
              if($hasHead == false){
                echo "<hr />";
              }
              $contact_id = get_post_meta( get_the_id(), "contact_id", true );
              echo do_shortcode( '[contact-form-7 id="' . $contact_id . '"]' ); 
      } ?>
      </div>


  </div>

</div>
<div class="">
    <?php echo emitShowcases($cms); ?>
</div>