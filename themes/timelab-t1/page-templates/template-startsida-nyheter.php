<?php
/*
Template Name: Nyhetsstartsida
*/

global $cms;

/*
 * A template for a front page with the following format.
 * 
 * ------------------------------------------------------
 * |													|
 * |					  Header						|
 * |													|
 * ------------------------------------------------------
 * |					   Menu 						|
 * ------------------------------------------------------
 * |													|
 * |													|
 * |													|
 * |					  Slider						|
 * |													|
 * |													|
 * |													|
 * ------------------------------------------------------
 * |								|					|
 * |								|					|
 * |								|					|
 * |		Front page text			|					|
 * |								|					|
 * |								|					|
 * |								|					|
 * ---------------------------------|					|
 * |								|	  Showcases		|
 * |								|					|
 * |								|					|
 * |		      News				|					|
 * |								|					|
 * |								|					|
 * |								|					|
 * |--------------------------------|-------------------|
 */
$site_url = get_template_directory_uri();
?>

<!--[if IE 8]>
<style>
.flex-direction-nav .flex-prev 
{
	left: -20px;
	height: 118px;
	background-image: url(<?php $site_url; ?>/assets/img/pilvansterIE.png);
}
.flex-direction-nav .flex-next 
{
	right: -21px!important;
	height: 118px;
	background-image: url(<?php $site_url; ?>/assets/img/pilhogerIE.png);
}
</style>
<![endif]-->

<div class="container slider">
    <script>
    	$(window).load(function() 
		{
			$('.flexslider').flexslider(
			{
				animation: "slide",
				prevText: '',
		        nextText: ''
			});
			$(".social").click(function() 
			{ 
				$(".socialPanel").toggleClass('hideMe'); 
			});
		});
	</script>
    
    <div class="row">
		<div class="col-sm-12">
			<?php
                $sliderId = get_post_meta(get_the_ID(), 'slider_id', true);
                if (!empty($sliderId)) {
                    echo emitSlider($cms, $sliderId);
                }
            ?>
		</div>
	</div>
</div>


<div class="wrap container mainText" role="document">
	<div class="row">
	    <div class="col-sm-12 startpage">
	        <?php get_template_part('templates/page', 'header'); ?>
	    </div>
	    
		<div class="col-sm-8 startpagetext">
			<?php get_template_part('templates/content', 'page'); ?>
	    </div>
	    
	    <div class="col-sm-4">
			<?php echo emitShowcases_vertical($cms); //exists in t1-lib.php ?>
		</div>
	</div>
</div>