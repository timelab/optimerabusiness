<?php
  /*
  Template Name: Landingpage Template
  */
  global $cms;

  // Add responsive container to embeds
  function mk_embed_html( $html ) {
    return '<div class="video-wrapper">' . $html . '</div>';
  }
  add_filter( 'embed_oembed_html', 'mk_embed_html', 10, 3 );
  add_filter( 'video_embed_html', 'mk_embed_html' ); // Jetpack

  $footer_contact = "";
  $home_url = get_bloginfo( 'url' );
  $home_url_name = (strpos($home_url, 'localhost')) ? "www.localhost.com" : preg_replace('#^http(s)?://#', "", $home_url);

  // For footer contact
  $facilities = $cms->getApi('Contact')->getFacilities();
  foreach ($facilities as $facility){
    $address = $facility->getAddress();
    
    foreach ($facility->getContactDetails() as $fUppgift){
      switch ($fUppgift->getType()) {
        case 'email':
          $email = $fUppgift->getFormatted();
          break;
        case 'telephone':
          $phone = $fUppgift->getFormatted();
          break;
      }
    }

    $footer_contact .= buildFooterContact($address->getStreet(), $address->getZip(), $address->getCity(), $phone, $email, $home_url, $home_url_name);
  }

  function buildFooterContact($address, $zip, $city, $phone, $email, $home_url, $home_url_name){
    $html = <<<HTML
      <div class="contact">
      {$address}, {$zip} {$city}, {$phone}<br>
      <strong><a href="{$home_url}">{$home_url_name}</a>, {$email}</strong>
      </div>
HTML;

    return $html;
  }

  // Convert hex color to rgba
  function hex2rgba($color, $opacity = 100){
    $color = trim($color, "#");
 
    $hex = hexdec($color);
 
    if( strlen($color) == 6 ){
      $r = hexdec( substr($color, 0, 2) );
      $g = hexdec( substr($color, 2, 2) );
      $b = hexdec( substr($color, 4, 2) );

      if( $opacity > 99){
          $opacity = 1;
      }else{
          $opacity = "0.$opacity";
      }

      $a = $opacity;
    }
 
    else{
        return "Error color code! Please enter correct color code, for example #ffffff";
        return false;
    }
 
    return $r.", ".$g.", ".$b.", ".$a;
   
  }


  //head and some random variable
  $site_url = get_site_url();
  get_template_part('templates/head');
?>

<body class="landingpage">
  <header>
    <img class="header-logo" src="<?= get_field( 'acf_landingpage_header_logo' ); ?>" />
  </header>

  <div class="image-devider" style="background-image: url('<?= get_field( 'acf_landingpage_devider_img' ); ?>')"></div>

  <div class="container main-content" role="document">
    <div class="row relative">
      <div class="col-xs-12 page-header">
       
      </div>
    </div>
      
    <div class="row relative minPageHeight">
      <div class="col-xs-12">
        <?php get_template_part('templates/content', 'page'); ?>

        <hr />

        <div class="home-button">
          <a class="landing-button" href="<?= $home_url ?>">Besök <?= $home_url_name ?></a>
        </div>
      </div>
    </div>
  </div>

  <div class="tempalte-cf-message">
    <h2><?= (get_field( 'acf_landingpage_form_success_header' )) ? get_field( 'acf_landingpage_form_success_header' ) : "Tack för ditt meddelande!" ?></h2>
    <div class="message"><?= (get_field( 'acf_landingpage_form_success_text' )) ? get_field( 'acf_landingpage_form_success_text' ) : "Vi tar kontakt med dig inom kort." ?> </div>
    <img src="<?= get_template_directory_uri(); ?>/assets/img/landingpage/message-sent.png" />
  </div>

  <footer style="background-color: <?= get_field( 'acf_landingpage_footer_color' ); ?>;">
    <div class="container">
      <div class="row">
        <div class="col-sm-6 left">
          <img class="footer-logo" src="<?= get_field( 'acf_landingpage_footer_logo' ); ?>" />
        </div>
        <div class="col-sm-6 right">
          <h1><?= (get_field( 'acf_landingpage_footer_title' )) ?  get_field( 'acf_landingpage_footer_title' ) : get_bloginfo( 'name' ); ?></h1>
          <?= $footer_contact; ?>
        </div>
      </div>
    </div>
    <?php wp_footer(); ?>

    <!-- Custom styles that you set in admin for other everyother syles go to the less file !-->
    <style type="text/css">
      .landing-button{
        background-color: <?= get_field( 'acf_landingpage_accent_color' ); ?> !important;
      }

      input[type="submit"]:disabled{
        background-color: rgba(<?= hex2rgba(get_field( 'acf_landingpage_accent_color' ), 50) ?>) !important;
      }
    </style>


    <!-- Scripts on the page !-->
    <script type="text/javascript">
      $('.tempalte-cf-message').prependTo(".wpcf7");
      $('.tempalte-cf-message').addClass('cf-success-msg');
      $('.tempalte-cf-message').removeClass('tempalte-cf-message');

      $(".wpcf7").on('wpcf7:mailsent', function(event){
        $('.cf-success-msg', this).slideDown('400');
        goog_report_conversion();
      });

      $(document).ready(function(){
        checkFormFields();

        var footerHeight = $('footer .right').height()+"px";


        $('footer .left').css('line-height', footerHeight);
        

        $('.wpcf7 input[type="submit"]').parent().addClass('submit-container')

        $(":input").on('keyup', function() {
          checkFormFields();
        });

        function checkFormFields(){
          $(":input").each(function() {
            if($(this).attr('aria-required')){
              if($(this).val() === ""){
                $('.wpcf7 input[type="submit"]').attr("disabled","disabled");
                return false;
              }else{
                $('.wpcf7 input[type="submit"]').removeAttr("disabled");
              }
            }
          });
        }
      });

       /* <![CDATA[ */
      goog_snippet_vars = function() {
        var w = window;
        w.google_conversion_id = <?= get_field( 'acf_landingpage_google_tracking_id' ); ?>;
        w.google_conversion_language = "en";
        w.google_conversion_format = "3";
        w.google_conversion_color = "ffffff";
        w.google_conversion_label = "<?= get_field( 'acf_landingpage_google_tracking_label' ); ?>";
        w.google_remarketing_only = false;
      }
      // DO NOT CHANGE THE CODE BELOW.
      goog_report_conversion = function(url) {
        goog_snippet_vars();
        window.google_conversion_format = "3";
        var opt = new Object();
        opt.onload_callback = function() {
        if (typeof(url) != 'undefined') {
          window.location = url;
        }
      }
      var conv_handler = window['google_trackConversion'];
      if (typeof(conv_handler) == 'function') {
        conv_handler(opt);
        }
      }
      /* ]]> */
    </script>

    <script type="text/javascript"
    src="//www.googleadservices.com/pagead/conversion_async.js">
    </script>

  </footer>
</body>
</html>