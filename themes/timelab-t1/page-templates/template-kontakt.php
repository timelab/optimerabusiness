<?php
/**
 *	Template Name: Kontaktsida Mall
 */
$site_url = get_site_url();
?>







<?php
global $cms;
global $t1config;

$tabs = '';

$panels = '';
$active = 'active';
$facilities = $cms->getApi('Contact')->getFacilities();
$jscript = '';
$repArr = array("(0)", "-", " "); //array för att fixa till telefonnr i länkar.

/** @var $facility Timelab\Cms\Objects\Facility */
foreach ($facilities as $facility)
{
	$address = $facility->getAddress();
	$openinghours = $facility->getOpeningHours();
	$contactDetails = $facility->getContactDetails();

	$tabs .= "<li class='" . str_replace('in ', '', $active) . "'>";
	$tabs .= "<a href='#{$facility->getSlug()}' data-toggle='tab' data-lat='{$address->getLat()}' data-long='{$address->getLong()}'>";
	$tabs .= $facility->getTitle();
	$tabs .= "</a>";
	$tabs .= "</li>";

	//Personal & Karta
	$map = '<div id="map-canvas" style="height: 310px"></div>';
	$map .= '<div class="mapBottom"></div>';

  $staff .= '<hr />';

	$staff .='<div class="staff-list">';
	foreach ($facility->getPersonnel() as $person)
	{
		$staff .= '<div class="col-xs-6 col-md-3 col-sm-4 personal">';
		if ($person->getImage() !== null)
		{
			$staff .= "<img src='" . $person->getImage()->getSrc() . "' alt='{$person->getName()}' />";
		}

		$staff .= "<div class='PL_infosquare'>";
		$staff .= "<div class='PL_name'>{$person->getName()}</div>";
		$staff .= "<div class='PL_role'>{$person->getRole()}</div>";

		$pUppgifter = $person->getContactDetails();
		foreach ($pUppgifter as $pUppgift)
		{
			if ($pUppgift->getValue() !== '')
			{
				$staff .= "<div class='PL_Uppgift PL_{$pUppgift->getType()}'>";
				if ($pUppgift->getType() === 'email')
				{
					$staff .= "<a href='mailto:{$pUppgift->getValue()}'>[E-post]</a>";
				}
				else
				{
					$staff .= "<span class='key'>Tel: </span>";
					$staff .= $pUppgift->getFormatted();
				}
				$staff .= "</div>";
			}
		}
		$staff .= "</div><!-- /infosquare -->";
		$staff .= "</div><!-- /personal -->";
	}
	$staff .= "<div class='clearfix'></div></div>";



	if($page_content == ''){
	    $page_content = apply_filters('the_content', $post->post_content);
	}

	//Kontaktinformation
	$contact ='<div class="row contact-column">';

	$contact .='<div class="col-xs-12 col-sm-6">';
	$contact .= "<div class='row'>";
	$contact .='<div class="col-xs-12 contactInfo">';
	$contact .= "<h3>Information</h3>\n";
	$contact .= $page_content;
	$contact .= '<hr />';
	$contact .='</div>';
	$contact .='<div class="col-xs-12 contactInfo">';
	$contact .= "<h3>Kontaktinformation</h3>\n";

	//Företagsuppgifter
	//Telefonnr, email
	foreach($contactDetails as $fUppgift)
	{
		if ($fUppgift->getValue() != '')
		{
			$contact .= ($fUppgift->getType() == 'email') ? "<span class='contactHeader'>Epost: </span>\n<span class='contactText'><a href='mailto:".$fUppgift->getValue()."'>".$fUppgift->getValue()."</a></span>\n" : "<span class='contactHeader'>Telefon: </span>\n<span class='contactText'><a href='tel:".str_replace($repArr,'',$fUppgift->getValue())."'>".$fUppgift->getValue()."</a></span>\n";
			$contact .= "<br />";
		}
	}

	$contact .= "<span class='contactHeader'>Adress:</span>\n";
	$contact .= "<span class='contactText'>";
	$contact .= "{$address->getStreet()}";
	$contact .= "</span><br />\n";
	$contact .='<div class="contactInfo-content"><hr />';
	$contact .= wpautop(get_the_content());
	$contact .= "</div>";
	$contact .= "</div>";

	$contact .= "</div>";
	$contact .="</div>";



	$contact .= '<div class="col-xs-12 col-sm-6">';
	$contact .= '<h3>Kontaktformulär</h3>';
	$contact .= do_shortcode('[contact-form-7 id="4" title="Contact form 1"]');
	$contact .= '</div>';
	$contact .= "</div>\n";

	$contact .= "<div class='clear'></div>\n";


	// Autobots assemble!
	$panels .= "<div class='tab-pane {$active}' id='{$facility->getSlug()}'>";
	$panels .= "<!-- Map and staff contact info -->";
	$panels .= "<div class='col-xs-12 subContent'>{$map}</div>";

	$panels .= "<!-- Contact details & form -->";
	$panels .= "<div class='col-xs-12 subBorderLeft'>{$contact}</div>";
	$panels .= "<div class='col-xs-12 subBorderLeft'>{$staff}</div>";
	$panels .= "</div>";

	//Script part for Lat/Long on first facility
	$jscript .= $active != '' ? "var mapLat = ".$address->getLat()."; var mapLong = ".$address->getLong()."; var mapAdress = '". $address->getStreet() ."'; var mapPnrCity = '".$address->getZip()." ".$address->getCity()."';" : "";

	$active = '';
}
?>


<div class="wrap container mainText subpage" role="document">
	<div class="row relative">
		<div class="col-xs-12 subHeader">
			<?php get_template_part('templates/page', 'header'); ?>
		</div>

		<script type="text/javascript">
			var responsiveMap;
			var beachMarker;
			var mapOptions;
			<?php echo $jscript; ?>
		</script>
		<div class="tab-content"><?php echo $panels; ?></div>
	</div>
</div>

<script type="text/javascript">
	$(".wpcf7").on('wpcf7:mailsent', function(event){
	  $.getScript("//www.googleadservices.com/pagead/conversion_async.js").done(function() {
	    goog_report_conversion(); 
	  });

  	/* <![CDATA[ */
    goog_snippet_vars = function() {
      var w = window;
      w.google_conversion_id = 857031706;
      w.google_conversion_language = "en";
      w.google_conversion_format = "3";
      w.google_conversion_color = "ffffff";
      w.google_conversion_label = "ibniCJbbgXEQmojVmAM";
      w.google_remarketing_only = false;
    }
    // DO NOT CHANGE THE CODE BELOW.
    goog_report_conversion = function(url) {
      goog_snippet_vars();
      window.google_conversion_format = "3";
      var opt = new Object();
      opt.onload_callback = function() {
      if (typeof(url) != 'undefined') {
        window.location = url;
      }
    }
    var conv_handler = window['google_trackConversion'];
    if (typeof(conv_handler) == 'function') {
      conv_handler(opt);
      }
    }
    /* ]]> */
	});

</script>