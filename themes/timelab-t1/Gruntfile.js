'use strict';
module.exports = function(grunt)
{
	grunt.initConfig(
	{
      theme_path: ".",
	    less:
	    {
	    	production:
	    	{
	    		options:
	    		{
	    			cleancss: true,
	    			compress: true
	    		},
	    		files:
	    		{
	    			'<%= theme_path %>/assets/css/main.min.css':
	    			[
	    			 	'<%= theme_path %>/assets/less/app.less'
	    			]
	    		}
	    	}
	    },
	    uglify:
	    {
	    	dist:
	    	{
	    		files:
	    		{
	    			'<%= theme_path %>/assets/js/scripts.min.js':
	    			[
			            '<%= theme_path %>/assets/js/_main.js',
			            '<%= theme_path %>/assets/js/_*.js',
			            '<%= theme_path %>/assets/js/plugins/_*.js',
				    '<%= theme_path %>/assets/js/plugins/bootstrap/tooltip.js',
			            '<%= theme_path %>/assets/js/plugins/bootstrap/*.js'
		            ]
	    		}
	    	}
	    },
	    watch:
	    {
	    	less:
	    	{
	    		files:
	    		[
	    		 	'<%= theme_path %>/assets/less/*.less',
	    		 	'<%= theme_path %>/assets/less/templates/*.less',
	    		 	'<%= theme_path %>/assets/less/page-templates/*.less',
	    		 	'<%= theme_path %>/assets/less/bootstrap/*.less'
	    		],
	    		tasks: ['less']
	    	},
	    	js:
	    	{
	    		files:
	    		[
					'Gruntfile.js',
					'<%= theme_path %>/assets/js/*.js',
					'<%= theme_path %>/assets/js/plugins/*.js',
					'!<%= theme_path %>/assets/js/scripts.min.js'
	    		],
	    		tasks: ['uglify']
	    	}
		},
	    clean:
	    {
	    	dist:
	    	[
	    	 	'<%= theme_path %>/assets/css/main.min.css',
	    	 	'<%= theme_path %>/assets/js/scripts.min.js'
	    	]
		}
	});


	// Load tasks
	//grunt.loadTasks('tasks');
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');

	// Register tasks
	grunt.registerTask('default',
	[
	    'less',
	    'uglify'
    ]);
	grunt.registerTask('dev',
	[
	 	'watch'
	]);
};
