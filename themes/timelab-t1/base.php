<?php
    global $cms;
    global $t1config;
$toReplace = array(".php","page-templates/");
$template_name = str_replace($toReplace, "", get_post_meta($wp_query->post->ID, '_wp_page_template', true));
if($template_name == "template-landingpage"){
	include roots_template_path();
}else{
    get_template_part('templates/head');
?>

<body <?php body_class(); ?>>
	<div class="container">
		<?php 
			get_template_part('templates/header');
	
			do_action('get_header');
			// Use Bootstrap's navbar if enabled in config.php
	     	if (current_theme_supports('bootstrap-top-navbar')) 
			{
	        	get_template_part('templates/header-top-navbar');
	      	} 
	      	else 
			{
	        	echo "<h2>Warning! Please enable the bootstrap navbar in config.php</h2>";
	      	}
	    ?>
	</div>  
	<?php include roots_template_path(); ?>
  	<?php get_template_part('templates/footer'); ?>
</body>
</html>
<?php }?>