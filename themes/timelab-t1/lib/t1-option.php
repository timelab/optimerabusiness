<?php

// create custom plugin settings menu
add_action('admin_menu', 'custom_settings_create_menu');

function custom_settings_create_menu() {

  //create new top-level menu
  add_menu_page('Custom settings', 'Custom settings', 'administrator', "custom-option", 'custom_settings_page' );

  //call register settings function
  add_action( 'admin_init', 'register_custom_settings' );
}


function register_custom_settings() {
  //register our settings
  register_setting( 'custom-settings-group', 'kontakt_page' );
  register_setting( 'custom-settings-group', 'disable_gallery' );
  register_setting( 'custom-settings-group', 'footer_link_text' );
}

function custom_settings_page() {
  //Grab pages
  $args = array(   
    'post_type'        => 'page',
    'post_status'      => 'publish',
  );
  $posts = get_posts($args);

  // For Select about contact page
  $select_html = "<option value=''>--Select one--</option>";
  foreach ($posts as $post) { 
  $select_html .= ($post->ID == get_option('kontakt_page')) ? "<option value='{$post->ID}' selected>{$post->post_title}</option>" : "<option value='{$post->ID}'>{$post->post_title}</option>";
  }

  // For checkbox about disable gallery
  $diGa = (get_option('disable_gallery')) ? " checked": "";

  // For input about footer link text
  $foLi = (get_option('footer_link_text') != null) ? get_option('footer_link_text') : "";
?>
<div class="wrap">
<h2>Custom settings</h2>

<form method="post" action="options.php">
    <?php settings_fields( 'custom-settings-group' ); ?>
    <?php do_settings_sections( 'custom-settings-group' ); ?>
    <table class="form-table">
        <tr valign="top">
        <th scope="row">Kontakt page: </th>
        <td>
          <select name="kontakt_page">
            <?= $select_html ?>
          </select>
        </td>
        </tr>
        <tr valign="top">
        <th scope="row">Footer link text: </th>
        <td>
          <input type="text" name="footer_link_text" value="<?= $foLi ?>"/>
        </td>
        </tr>
        <tr valign="top">
        <th scope="row">Disable gallery: </th>
        <td>
          <input type="checkbox" name="disable_gallery"<?= $diGa ?>>
        </td>
        </tr>
    </table>
    
    <?php submit_button(); ?>

</form>
</div>
<?php 
} 

if(get_option('disable_gallery')){
  function post_remove () { 
    remove_menu_page('timelab_cms_gallery');
  } 

  add_action('admin_menu', 'post_remove');
}

add_filter('gettext', 'rename_admin_menu_items');
add_filter('ngettext', 'rename_admin_menu_items');

function rename_admin_menu_items( $menu ) {
  
  // $menu = str_ireplace( 'original name', 'new name', $menu );
  $menu = str_ireplace( 'Contact', 'Kontaktformulär', $menu );
  
  // return $menu array
  return $menu;
}

function custom_menu_order($menu_ord) {
  if (!$menu_ord) return true;
  if(in_array("timelab_cms_Contact", $menu_ord)){
    for($i = 0; $i < count($menu_ord); $i++){
      if($menu_ord[$i+1] == 'timelab_cms_Contact'){
        break;
      }

      if($menu_ord[$i] == "wpcf7"){
        $menuItem = $menu_ord[$i];
        $menu_ord[$i] = $menu_ord[$i+1];
        $menu_ord[$i+1] = $menuItem;
      }
    }
  }
  return $menu_ord;
}
add_filter('custom_menu_order', 'custom_menu_order'); // Activate custom_menu_order
add_filter('menu_order', 'custom_menu_order');