<?php

// Add Shortcode
function boka_direkt_button_shortcode( $atts ) {

    // Attributes
    extract( shortcode_atts(
            array(
                'sru' => '',
            ), $atts )
    );

    // Code
    return '<script>
            (function (){var bd = document.createElement("script"); bd.type = "text/javascript"; bd.async = true;bd.src = "http://foretag.bokadirekt.se/bd.min.js";var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(bd, s);})();
            </script>
            <a class="bd-button" title="Boka tid på Bokadirekt.se" href="http://www.bokadirekt.se/Bookings/Default.aspx?sru='. $sru . '" target="_blank" data-sru="'. $sru . '">Boka tid</a>
            ';

}
add_shortcode( 'boka_direkt_button', 'boka_direkt_button_shortcode' );

function cateringDisplay($atts){
	extract(shortcode_atts(array(
		"typ" => 'typ',
		"rubrik" => 'Rubrik'
	), $atts));

	$html = "";

	$podparams = array('where' => 'typ.meta_value="'.$typ.'"', 'orderby' => 'pris.meta_value ASC', 'limit' => 999);
	$objectList = pods('catering', $podparams);

	if ($objectList->total() > 0){
		$html = "<div class='cateringHeader'><h4>" . $rubrik . "</h4></div>";
		while ($objectList->fetch())
		{
			$html.= "<div class='row cateringItem'>\n";
				$html .= "<div class='menurowpadd'>\n";
				$html .= "<div class='col-xs-12 col-sm-10 cateringTitle'>". $objectList->field('post_title') . "</div>";
				$html .= "<div class='col-xs-12 col-sm-2 cateringPrice'>" .$objectList->field('pris') . ":-</div>";
				if($objectList->field('post_content')!=''){
					$html .= "<div class='col-xs-12 col-sm-10 cateringDescription'>" .strip_tags($objectList->display('post_content'),'<br><br/>') . "</div>";
				}
				$html .= "</div>\n";
			$html .= "</div>\n";
		}
	}


	return $html;
}
add_shortcode( 'catering', 'cateringDisplay' );


function emitBrands($cms)
{
	global $t1config;
	$html = "";
	
	if ($t1config['brands_enabled'] === false)
	{
		return $html;
	}
	
	$podparams = array('where' => 'synlig.meta_value=True', 'orderby' => 'ordning.meta_value ASC,id DESC', 'limit' => 6);
	$objectList = pods('brands', $podparams);
	if ($objectList->total() > 0)
	{
		$html .= "<div class='container showcases brands PodsPuffar {$t1config['hide_brands']}'>";
			$html .= "<div class='row'>";

			$classes = "";
			switch ($objectList->total())
			{
				case 1:
				case 2:
				case 3:
					$classes = "col-xs-4 col-sm-4 col-md-4";
					break;
				case 4:
					$classes = "col-xs-6 col-sm-3 col-md-3";
					break;
				case 5:
				case 6:
					$classes = "col-xs-6 col-sm-2 col-md-2";
					break;
				default:
					$classes = "col-xs-6 col-sm-2 col-md-2";
			}
			$border = "border";

			$i = 0;
			while ($objectList->fetch())
			{
				$i++;
				if ($i === $objectList->total()) { $border = ""; }

				$linkArr = $objectList->field('puff_lank');

				$html .= ($linkArr !== "") ? "<a href='{$linkArr}' target='_blank'>" : "";
				$html .= "<span class='{$classes} {$border} brand'>";
					$html .= "<span class='brand-bg'>";
						$html .= '<img src="' . $objectList->display('puff_bild') . '"/>';
						
						if (!empty($objectList->display('puff_bild_hover')))
						{
							$html .= '<img class="hover" src="' . $objectList->display('puff_bild_hover') . '"/>';
						}
					$html .= '</span>';
				$html .= '</span>';
				$html .= ($linkArr !== "") ? '</a>' : '';
			}
			$html .= "</div>";
		$html .= "</div>";
	}

	return $html;
}


function emitShowcases($cms)
{
	$html = "";

	$podparams = array('where' => 'synlig.meta_value=True', 'orderby' => 'ordning.meta_value ASC,id DESC', 'limit' => 4);
	$objectList = pods('puffar', $podparams);
	if ($objectList->total() > 0)
	{
		$count = 0;

        $html .= "<div class='container showcases PodsPuffar'>";
        $html .= "<div class='row showcase-row'>";
        while ($objectList->fetch())
        {
            $count++;
            $linkArr = $objectList->field('puff_lank');
            $linkTarget = '_self';
            if(!empty($linkArr)){
                $podpermalink = get_permalink($linkArr['ID']);
            }else{
                $podpermalink = $objectList->field('extern_lank');
                $linkTarget = "_blank";
            }

            $html .= "<a href='$podpermalink' target='$linkTarget'>";
            $html .= "<span class='col-sm-4 showcase pos$count'>";
            $html .= "<span class='showcase-bg'>";
            $html .= "<img src='{$objectList->display('puff_bild')}' alt='{$objectList->field('puff_text')}'/>";
            if (!empty($objectList->display('puff_bild_hover')))
            {
                $html .= "<img class='hover' src='{$objectList->display('puff_bild_hover')}' alt='{$objectList->field('puff_text_hover')}'/>";
            }
            $html .= "<span class='showcase-title'>{$objectList->field('puff_text_rubrik')}</span>";
            if ($objectList->field('puff_text') !== "")
            {
                $html .= "<span class='showcase-text'>{$objectList->field('puff_text')}</span>";
            }
            $html .= "</span>";
            $html . "</span>";
            $html .= "</a>";
        }
        $html .= "</div>";
        $html .= "</div>";
	}

	return $html;
}

function emitShowcases_vertical($cms)
{
	$podparams = array('where' => 'synlig.meta_value=True', 'orderby' => 'ordning.meta_value ASC,id DESC', 'limit' => 4);
	$objectList = pods('puffar', $podparams);

    $html = '<div class="showcases showcases-vertical">';
    if ($objectList->total() > 0)
    {
		// loop through items using pods::fetch
		$count = 0;
		while ($objectList->fetch())
        {
			$count++;
			$linkArr = $objectList->field('puff_lank');
			$podpermalink = get_permalink($linkArr['ID']);

			$html .= "<div class='col-xs-6 col-sm-6 col-md-12 showcase showcase-vertical pos$count'>";
				$html .= "<a href='$podpermalink' class='showcase-href'>";
					$html .= '<span class="showcase-bg">';
						$html .= '<img src="'.$objectList->display('puff_bild').'" alt="'.$objectList->field('puff_text').'" />';
						if (!empty($objectList->display('puff_bild_hover')))
						{
							$html .= "<img class='hover' src='{$objectList->display('puff_bild_hover')}' alt='{$objectList->field('puff_text_hover')}'/>";
						}
						$html .= "<span class='showcase-title'>{$objectList->field('puff_text_rubrik')}</span>";
						if ($objectList->field('puff_text') !== "")
						{
							$html .= "<span class='showcase-text'>{$objectList->field('puff_text')}</span>";
						}
					$html .= '</span>';
				$html .='</a>';
			$html .= '</div>';
		}
	}
    $html .='</div>';
    return $html;
}


function emitSlider($cms, $sliderID)
{
	$html = "<div class='sliderShadow'></div>";
	$html .= "<div class='flexslider'>";
		$html .= "<ul class='slides'>";

        /** @var $slider Timelab\Cms\Objects\SliderObject */
        $slider = $cms->getApi('Slider')->getSlider($sliderID);

		foreach ($slider->getImages(true) as $slide)
		{
            $title      = $slide->getTitle();
            $subtitle   = $slide->getSubtitle();
            $text       = $slide->getText();

            $slideHtml = "";

            if ( !(empty($title) && empty($subtitle) && empty($text))  )
            {
                $slideHtml .= "<div class='slideText'>";

                    $href       = $slide->getHref();

                    $slideHtml .= empty($title) ? "" : "<h2>{$title}</h2>";
                    $slideHtml .= empty($subtitle) ? "" : "<h3>{$subtitle}</h3>";
                    $slideHtml .= empty($text) ? "" : "<p>{$text}</p>";
                $slideHtml .= "</div>";
            }

            $imageSrc = wp_get_attachment_image_src($slide->getMediaId(), array(1018, 636));
            $slideHtml .= "<img src='{$imageSrc[0]}'/>";

			$html .=  "<li>";
				$html .= empty($href) ? $slideHtml : "<a href='{$href}' target='{$slide->getTarget()}'>$slideHtml</a>";
			$html .= "</li>";
		}
		$html .= "</ul>";
	$html .= "</div>";
	
	return $html;
}


function emitSocial($cms, $direct = false)
{
  global $t1config;
  $html = "";
  $class = "";

  $social_array = SocialList($cms, true);
  
  if ($t1config['social_integration_enabled'] === false)
  {
    return $html;
  }
  
  $html .= "<div>";
    foreach ($social_array as $social => $link) {
      if(($direct == false) && ($social == 'facebook' || $social == 'instagram' )){
        $link = "#";
      }else{
        $class = ' direct';
      }

      $html .= "<a href='{$link}' class='top-icon {$social}Btn{$class}' target='_blank'>";
      $html .= "<img src='" . get_template_directory_uri() . "/assets/img/top_" . $social . ".png' alt='" . $social . "'/>";
      $html .= "</a>";
    }

  $html .= "</div>";
  
  return $html;
}


function getContactInfo($cms)
{
	// Loop through facilities to gather data.
	$facilities = array();
	foreach ($cms->getApi('Contact')->getFacilities() as $facility)
	{	
		$address = $facility->getAddress();
	
		$data = array();
		$data['address'] = $address->getStreet() . ' ' . $address->getZip() . ' ' . $address->getCity();
		$data['address_2'] = $address->getStreet() . ' ' . $address->getCity();
		foreach ($facility->getContactDetails() as $fUppgift)
		{
			if ($fUppgift->getValue() !== '')
			{
				$data[$fUppgift->getType()] = $fUppgift->getValue();
			}
		}
		$facilities[] = $data;
	}
	
	return $facilities;
}

function SocialList($cms, $asArray = false){
  $facilities_detail = $cms->getApi('Contact')->getFacilities();

  $facility_detail = $facilities_detail[0];

  if($facility_detail == null){
    return array();
  }

  $socials = $facility_detail->getSocialDetails();
  $social_li = "";
  $social_arr = array();

  foreach ($socials as $social) {
    if ($social->getValue() != ''){
      if($asArray){
        $social_arr[$social->getType()] = $social->getFormatted();
        $output = $social_arr;
      }else{
        $type = "";
        switch ($social->getType()) {
          case 'facebook':
            $social_li .= "<li id='" . $social->getType() . "'><a href='" . $social->getFormatted() . "'>Facebook</a>";
            break;
          case 'instagram':
            $social_li .= "<li id='" . $social->getType() . "'><a href='" . $social->getFormatted() . "'>Instagram</a>";
            break;
          case 'linkedin':
            $social_li .= "<li id='" . $social->getType() . "'><a href='" . $social->getFormatted() . "'>Linkedin</a>";
            break;
          case 'youtube':
            $social_li .= "<li id='" . $social->getType() . "'><a href='" . $social->getFormatted() . "'>Youtube</a>";
            break;
        }
        $output = $social_li;
      }
    }
  }

  return $output;
}
