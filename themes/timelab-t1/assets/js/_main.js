// Modified http://paulirish.com/2009/markup-based-unobtrusive-comprehensive-dom-ready-execution/
// Only fires on body class (working off strictly WordPress body_class)

var ExampleSite = {
  // All pages
  common: {
    init: function() {


        //Facebook
        $('.facebookBtn').click(function(e){
          if(! $(this).hasClass('direct')){
            e.preventDefault();
            toggleSocial($('div.facebook'));
          }
        });


        $('.instagramBtn').click(function(e){
          if(! $(this).hasClass('direct')){
            e.preventDefault();
            toggleSocial($('div.instagram'));
          }
        });


        $('.socialPane .socialClose').click(function(){
            $(this).parent().parent().removeClass('open');
        });

        function toggleSocial($target){
            if($target.hasClass('open')){
                $target.removeClass('open');
            }else{

                //Stäng ev. öppna panels
                $('.socialPane.open').removeClass('open');

                //Öppna denna
                $target.addClass('open');
            }
        }

      // JS here
        //Lägg till en clone av huvudmenyval när det finns dropdowns
        //Loopa alla a med klassen dropdown-toggle
        $('.navbar-nav li:last-child a.dropdown-toggle').each(function(){
            //Clona first-child i submeny (Borde alltid finnas åtminstone en eftersom det är en dropdown)
            $cloned = $('ul li:first-child',$(this).parent()).clone();

            //Ta klasserna från huvudmenyval men exkludera dropdownklassen
            var $classes = $(this).parent().attr('class').replace('dropdown','');

            //Om det finns en annan submeny som är aktiv, ta bort även active classen som kommer från huvudmenyval då en submeny är active
            $classes = $(this).parent().find('ul li.active').length > 0 ? $classes.replace('active', ''):$classes;

            //Modifiera classer,href och text på clonen för att spegla huvudmenyval
            $cloned.attr('class',$classes).find('a').attr('href',$(this).attr('href')).text($(this).text());

            //Prependa den så den hamnar först i submenyn
            $('ul',$(this).parent()).prepend($cloned);
        });

        $('.wpcf7-form-control-wrap .wpcf7-form-control').each(function(){
            $(this).focus(function(){
                $(this).parent().find('span').fadeOut();
            });
        });

        $('.wpcf7 .help-box').each(function(){
          $('img', this).attr("src", baseUrl+"/assets/img/infoIcon.png");
          $(this).popover();
        })

        $('.wpcf7 .input-group .text-checkbox').on('click', function(){
          $sibling = $(this).siblings('.wpcf7-form-control-wrap');
          $target = $('.wpcf7-checkbox .wpcf7-list-item input', $sibling)
          if(!$target.is(':checked')){
            $target.prop('checked', true);
          }
          else{
            $target.prop('checked', false);
          }
        })

        $(".wpcf7-radio .wpcf7-list-item").click(function()
        { 
          var $hidden = $(this).parent().parent().parent().parent().siblings('.hidden-noimp');
          if($(this).hasClass('first')){
            if($('input',this).is(':checked')){
              $hidden.slideDown();
            }
          }
          else{
            if($hidden.hasClass('hidden-noimp')){
              $hidden.slideUp();
            }
          }
        });

        

    },
    finalize: function() { }
  },
  // Home page
  home: {
    init: function() {
      // JS here
    }
  },
  // About page
  about: {
    init: function() {
      // JS here
    }
  }
};

var UTIL = {
  fire: function(func, funcname, args) {
    var namespace = ExampleSite;
    funcname = (funcname === undefined) ? 'init' : funcname;
    if (func !== '' && namespace[func] && typeof namespace[func][funcname] === 'function') {
      namespace[func][funcname](args);
    }
  },
  loadEvents: function() {

    UTIL.fire('common');

    $.each(document.body.className.replace(/-/g, '_').split(/\s+/),function(i,classnm) {
      UTIL.fire(classnm);
    });

    UTIL.fire('common', 'finalize');
  }
};

$(document).ready(UTIL.loadEvents);
