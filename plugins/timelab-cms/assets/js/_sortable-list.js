function update_order(event, ui) {

    $list = ui.item.parent();

    var order = 0;
    $list.find('input[name*="order"]').each(function (index) {
        if ($(this).val() !== '-1' && $(this).data('list-key') === $list.data('list-key')) {
            $(this).val(order);
            $(this).trigger('change');
            order++;
        }
    });
}

$(function () {
    $('ol.sortable-list').sortable({
        doNotClear: true,
        tabSize: 10,
        forcePlaceholderSize: true,
        placeholder: 'placeholder',
        update: function(event, ui) {
            update_order(event, ui);
        },
        tolerance: 'pointer',
        toleranceElement: '> div',
        items: 'li:not(.sort-disabled)'
    });

    $('ol.sortable-list a.remove-button').click(function(e) {

        $input = $(this).siblings('ul input[name*=remove]');

        if ($input.val() !== 'true') {
            $input.val('true');
            $input.closest('li').addClass('removed');
        } else {
            $input.val('');
            $input.closest('li').removeClass('removed');
        }

        $input.trigger('change');

        e.preventDefault();
    });
});