<?php

require_once('contact_detail.php');

/**
 * Class person
 * En person består bland annat av namn, roll, bild och kontaktuppgifter. Används oftast i personal listningar
 */
class person {

    /**
     * @var int Den unika identifiern för personen
     */
    public $ID;

    /**
     * @var int Ordningen personen ska dyka upp i. 0 är först
     */
    public $order;

    /**
     * @var string Namnet på personen (Tex: "Patrik Riström")
     */
    public $name;

    /**
     * @var string Personens roll på företaget (Tex: "Systemutvecklare")
     */
    public $role;

    /**
     * @var string Personens avdelning, använd dessa för att gruppera personal (Tex: "Försäljning")
     */
    public $department;

    /**
     * @var string Beskrivning på personen
     */
    public $description;

    /**
     * @var int Unika identifiern för media objektet för personbilden
     */
    public $media_id;

    /**
     * @param $ID int Den unika identifiern för personen
     * @param $order int Ordningen personen ska dyka upp i. 0 är först
     * @param $name string Namnet på personen (Tex: "Patrik Riström")
     * @param $role string Personens roll på företaget (Tex: "Systemutvecklare")
     * @param $department Personens avdelning (Tex: "Utveckling")
     * @param $description Personens beskrivning
     * @param $media_id int Unika identifiern för media objektet för personbilden
     */
    function __construct($ID, $order, $name, $role, $department, $description, $media_id) {
        $this->ID = $ID;
        $this->order = $order;
        $this->name = $name;
        $this->role = $role;
        $this->department = $department;
        $this->description = $description;
        $this->media_id = $media_id;
    }

    /**
     * Hämtar ut samtliga kontaktdetaljer för personen.
     * @return contact_detail[] Array med samtliga kontaktdetaljer för personen
     */
    public function get_contact_details() {

        if (empty($this->ID)) return array();

        // Hämta ut samtliga 'timelab_cms_contact' poster
        $posts = get_posts(array(
            'post_type' => 'timelab_cms_contact',
            'posts_per_page' => -1,
            'post_parent' => $this->ID,
            'meta_key' => 'order',
            'orderby' => 'meta_value_num',
            'order' => 'ASC'
        ));

        $contact_details = array();

        // Loopa igenom posterna och skapa contact_detail objekt
        foreach ($posts as $post) {
            $contact_detail = new contact_detail(
                $post->ID,
                get_post_meta($post->ID, 'order', true),
                get_post_meta($post->ID, 'type', true),
                get_post_meta($post->ID, 'value', true),
                get_post_meta($post->ID, 'name', true)
            );

            $contact_details[] = $contact_detail;
        }

        return $contact_details;
    }

    /**
     * Hämtar bilden för personen
     * @return string Urlen för bilden
     */
    public function get_image() {
        if (isset($this->media_id) && !empty($this->media_id)) {
            return wp_get_attachment_image_src($this->media_id, 'personnel', false)[0];
        } else {
            return null;
        }
    }

} 