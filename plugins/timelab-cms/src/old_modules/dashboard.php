<?php

/**
 * Class dashboard
 * Rensar upp dashboarden och lägger in våra egna boxar
 */
class dashboard extends cms_module {

    var $quick_buttons = array();

    /**
     * Konstruktorn, hookar in våra funktioner in i Wordpress
     */
    public function __construct() {
        parent::__construct();


        add_action( 'admin_menu', array($this, 'create_dashboard' ) );
        add_action('load-index.php', array( $this, 'redirect_to_dashboard' ) );

    }

    function redirect_to_dashboard() {
        wp_redirect(admin_url('admin.php?page=dashboard'));
    }

    function create_dashboard() {
        add_menu_page( '', 'Startsida', 'read', 'dashboard', array($this, 'output_dashboard'), 'dashicons-admin-site', 2 );
    }

    function output_dashboard() {
        $this->admin_page_template_top();
        echo "
            <h1>Välkommen till din adminpanel!</h1>
            <p>Detta är sidan du kommer att se varje gång när du loggar in på din webbplats!</p>";

        /*echo "<div class='row'>";
        $this->output_timelab_cms_news();
        $this->output_timelab_cms_news();
        echo "</div>";*/

        echo "<hr />";


        echo "<div class='row'>";
        foreach ($this->quick_buttons as $quick_button) {
            $this->output_quick_button($quick_button);
        }
        echo "</div>";

        echo "<div class='row'>";

        echo "</div>";
        $this->admin_page_template_bottom();
    }


    /**
     * @param string $header Rubriken på knappen
     * @param string $text Texten som ska visas på knappen
     * @param string $url Url dit knappen ska gå
     * @param string $icon_class CSS klass som ska läggas på ikonen, tom sträng om det inte ska vara ikon
     * @param string $btn_class CSS klass som ska läggas på knappen (Tex 'btn-success')
     */
    public function add_dashboard_quick_button($header, $text, $url, $icon_class = '') {
        $this->quick_buttons[] = array(
            "header" => $header,
            "text" => $text,
            "url" => $url,
            "icon_class" => $icon_class
        );
    }

    /**
     * Skapar innehållet för Timelab CMS Nyhets boxen
     */
    function output_timelab_cms_news() {
        echo "<div class='col-sm-6 puff'>
                <div class='puff-content'>";
        echo "<h2>Timelab CMS Nyheter</h2>";
        echo "Inga nyheter";
        echo "</div></div>";

    }

    /**
     * Skapar innehållet för Google Analytics boxen
     */
    function google_analytics() {
        echo "0 besökare";
    }

    /**
     * Skapar innehållet för Snabb Länkar boxen
     */
    function output_quick_button($quick_button) {
        echo "<div class='col-sm-3 puff quick-link'>";
        echo "<a href='{$quick_button['url']}' class='puff-content'>";
        if (!empty($quick_button['icon_class'])) {
            echo "<span class='glyphicon {$quick_button['icon_class']}'></span> ";
        }
        echo "{$quick_button['header']} <p>{$quick_button['text']}</p></a>";
        echo "</div>";
    }

}