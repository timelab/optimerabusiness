<?php
namespace Timelab\Cms\Objects;


use Timelab\Cms\Cms;
use Timelab\Cms\DatabaseObjectAbstract;
use Timelab\Cms\ObjectInterface;

class Image extends DatabaseObjectAbstract {

    private $mediaId;

    private $src;

    private $href;
    private $target;

    private $subtitle;
    private $text;

    private $dateFrom;
    private $dateTo;


    /**
     * Creates a blank image object, to initialize an image object you should look at some of the
     * static helper methods like `Image::fromPost`
     */
    public function __construct() {

    }

    /**
     * The post type of the object in the database, used internally by the object when saving
     * @return string The post type of the object
     */
    public function getPostType()
    {
        return 'timelab_cms_image';
    }

    /**
     * Checks if user are allowed to save, or only administrators.
     * @return bool `true` if user can save, `false` if only admins can save the data.
     */
    public function canUserSave()
    {
        return true;
    }

    /**
     * Checks if the object is ready to be saved to the database, this is where all the validation lies.
     * @return bool `true` if object can be saved, `false` if not
     */
    public function validateSave()
    {
        if ($this->getMediaId() == (null||'')) {
            return false;
        }
        return true;
    }

    protected function loadFromPostFields()
    {
        $this->setMediaId(get_post_meta($this->getId(), 'media_id', true));
        $this->setSrc(wp_get_attachment_image_src($this->getMediaId(), array(9000, 9000), false)[0]);

        $this->setHref(get_post_meta($this->getId(), 'href', true));
        $this->setTarget(get_post_meta($this->getId(), 'target', true));

        $this->setText(get_post_meta($this->getId(), 'text', true));
        $this->setSubtitle(get_post_meta($this->getId(), 'subtitle', true));

        $this->setDateFrom(get_post_meta($this->getId(), 'date_from', true));
        $this->setDateTo(get_post_meta($this->getId(), 'date_to', true));
    }


    /**
     * Returns the image in the form of `<img src="{{ src }}" />`
     * @example `echo $imageObject` to print out the image
     * @return string String representation of the image
     */
    function __toString()
    {
        return '<img src="' . $this->getSrc() . '" />';
    }

    /**
     * Gets the ID of the wordpress media object associated with the image
     * @return int|null The id of the associated media object, `null` if not associated with media object
     */
    public function getMediaId() {
        return $this->mediaId;
    }

    /**
     * @param mixed $mediaId
     */
    public function setMediaId($mediaId)
    {
        $this->mediaId = $mediaId;
    }

    /**
     * Gets the image src link
     * @return string The src of the image
     */
    public function getSrc() {
        return $this->src;
    }

    /**
     * @param mixed $src
     */
    public function setSrc($src)
    {
        $this->src = $src;
    }

    /**
     * Gets the image link (if it's linked to something)
     * @return string Image link
     */
    public function getHref() {
        return $this->href;
    }

    /**
     * @param mixed $href
     */
    public function setHref($href)
    {
        $this->href = $href;
    }

    /**
     * If the image is linked, this returns the target (`_self` or `_blank`)
     * @return string Image target
     */
    public function getTarget() {
        return $this->target;
    }

    /**
     * @param mixed $target
     */
    public function setTarget($target)
    {
        $this->target = $target;
    }

    /**
     * If the image has a subtitle, return it
     * @return string The Subtitle
     */
    public function getSubtitle() {
        return $this->subtitle;
    }

    /**
     * @param mixed $subtitle
     */
    public function setSubtitle($subtitle)
    {
        $this->subtitle = $subtitle;
    }

    /**
     * If the image has text, return it
     * @return string The text
     */
    public function getText() {
        return $this->text;
    }

    /**
     * The associated text, used for instance in sliders etc.
     * @param string $text The text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * Gets the date from when to start displaying the image
     * @return string The date, in the following format: `YYYY-MM-DD`
     */
    public function getDateFrom()
    {
        return $this->dateFrom;
    }

    /**
     * Sets the date from when to start displaying the image
     * @param $dateFrom string The date, in the following format: `YYYY-MM-DD`
     */
    public function setDateFrom($dateFrom)
    {
        $this->dateFrom = $dateFrom;
    }

    /**
     * Gets the date from when to stop displaying the image
     * @return string The date, in the following format: `YYYY-MM-DD`
     */
    public function getDateTo()
    {
        return $this->dateTo;
    }

    /**
     * Sets the date from when to stop displaying the image
     * @param string $dateTo string The date, in the following format: `YYYY-MM-DD`
     */
    public function setDateTo($dateTo)
    {
        $this->dateTo = $dateTo;
    }

    /**
     * Runs after the save method, used to save all custom_fields and other misc data to the database.
     */
    protected function saveFields()
    {
        update_post_meta($this->getId(), 'text', $this->getText());
        update_post_meta($this->getId(), 'subtitle', $this->getSubtitle());
        update_post_meta($this->getId(), 'media_id', $this->getMediaId());
        update_post_meta($this->getId(), 'href', $this->getHref());
        update_post_meta($this->getId(), 'target', $this->getTarget());
        update_post_meta($this->getId(), 'date_from', $this->getDateFrom());
        update_post_meta($this->getId(), 'date_to', $this->getDateTo());
    }

    /**
     * Returns image as an associative array, recommended for use in templates etc
     */
    public function asArray() {
        return array(
            'id'        => $this->getId(),
            'media_id'  => $this->getMediaId(),
            'src'       => $this->getSrc(),
            'href'      => $this->getHref(),
            'target'    => $this->getTarget(),
            'text'      => $this->getText(),
            'title'     => $this->getTitle(),
            'subtitle'  => $this->getSubtitle(),
            'order'     => $this->getOrder(),
            'date_from' => $this->getDateFrom(),
            'date_to'   => $this->getDateTo()
        );
    }

    /**
     * Checks if a specific date is within the range of the image's From and To dates
     * @param null|string $date The date to check if in range, if `null` it will use today's date.
     * @return bool `true` if within range, `false` if not.
     */
    public function inDateRange($date = null) {
        if (empty($date)) {
            $date = date("Y-m-d");
        }

        $to = $this->getDateTo();
        $from = $this->getDateFrom();

        if (empty($to) || empty($from)) {
            return true;
        }

        return (
            (strtotime($date) >= strtotime($this->getDateFrom())) &&
            (strtotime($date) <= strtotime($this->getDateTo()))
        );

    }

}