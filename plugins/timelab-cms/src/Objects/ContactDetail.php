<?php

namespace Timelab\Cms\Objects;


use Timelab\Cms\DatabaseObjectAbstract;

class ContactDetail extends DatabaseObjectAbstract {

    private $value;
    private $type;

    /**
     * The post type of the object in the database, used internally by the object when saving
     * @return string The post type of the object
     */
    public function getPostType()
    {
        return "timelab_cms_contact";
    }

    /**
     * Checks if user are allowed to save, or only administrators.
     * @return bool `true` if user can save, `false` if only admins can save the data.
     */
    public function canUserSave()
    {
        return true;
    }

    /**
     * Checks if the object is ready to be saved to the database, this is where all the validation lies.
     * @return bool `true` if object can be saved, `false` if not
     */
    public function validateSave()
    {
        if ($this->getValue() == (null||'')) {
            if ($this->getId() == null) {
                return false;
            } else {
                $this->trash();
            }

        }
        return true;
    }

    /**
     * Runs after the loadFromPost method, used to get all custom_fields and other misc data from the database and apply
     * to the object.
     */
    protected function loadFromPostFields()
    {
        $this->setValue(get_post_meta($this->getId(), 'value', true));
        $this->setType(get_post_meta($this->getId(), 'type', true));
    }

    /**
     * Runs after the save method, used to save all custom_fields and other misc data to the database.
     */
    protected function saveFields()
    {
        update_post_meta($this->getId(), 'value', $this->getValue());
        update_post_meta($this->getId(), 'type', $this->getType());
    }


    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * Gets the contact detail type name
     * @return string
     */
    public function getType() {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }


    /**
     * Gets the formatted value of the contact detail.
     * @return string
     */
    public function getFormatted() {

        switch ($this->getType()) {
            case ContactDetailType::TELEPHONE:
            case ContactDetailType::FAX:
            case ContactDetailType::MOBILE:
                $raw = preg_replace("/[^0-9+]/", "", $this->getValue());
                return '<a href="tel:' . $raw . '">' . $this->getValue() . '</a>';
                break;
            case ContactDetailType::EMAIL:
                return '<a href="mailto:' . $this->getValue() . '">' . $this->getValue() . '</a>';
                break;
            default:
                return $this->getValue();
        }

    }

    public function asArray() {
        return array(
            'id'        => $this->getId(),
            'value'     => $this->getValue(),
            'type'      => $this->getType(),
            'formatted' => $this->getFormatted()
        );
    }
}

abstract class ContactDetailType {
    const TELEPHONE     = 'telephone';
    const MOBILE        = 'mobile';
    const FAX           = 'fax';
    const EMAIL         = 'email';

    /**
     * Gets all of the types as an associative array, Keys are the Type name while the value is the display name.
     * This is mainly for use inside the administration.
     * @return array All the Contact Detail Types
     */
    static public function getTypes() {
        return array(
            ContactDetailType::TELEPHONE => "Telefon",
            ContactDetailType::MOBILE => "Mobil",
            ContactDetailType::FAX => "Fax",
            ContactDetailType::EMAIL => "E-post"
        );
    }
}