<?php

namespace Timelab\Cms\Objects;
use Timelab\Cms\Cms;
use Timelab\Cms\DatabaseObjectAbstract;
use Timelab\Cms\Modules\Slider;
use Timelab\Cms\ObjectInterface;

require_once('Image.php');

/**
 * This is a slider as an object
 *
 * Class SliderObject
 * @package Timelab\Cms\Modules
 */
class SliderObject extends DatabaseObjectAbstract {

    private $titleSupport;
    private $subtitleSupport;

    public function __construct() {

    }

    /**
     * The post type of the object in the database, used internally by the object when saving
     * @return string The post type of the object
     */
    public function getPostType()
    {
        return "timelab_cms_slider";
    }

    /**
     * Checks if user are allowed to save, or only administrators.
     * @return bool `true` if user can save, `false` if only admins can save the data.
     */
    public function canUserSave()
    {
        return false;
    }

    /**
     * Checks if the object is ready to be saved to the database, this is where all the validation lies.
     * @return bool `true` if object can be saved, `false` if not
     */
    public function validateSave()
    {
        if ($this->getTitle() === null) {
            return false;
        }

        return true;
    }

    /**
     * Runs after the loadFromPost method, used to get all custom_fields and other misc data from the database and apply
     * to the object.
     */
    protected function loadFromPostFields()
    {
        $this->setTitleSupport(get_post_meta($this->getId(), 'enable_title', true) == 1 ? true : false);
        $this->setSubtitleSupport(get_post_meta($this->getId(), 'enable_subtitle', true) == 1 ? true : false);
    }

    /**
     * Checks if slider supports titles on slider images
     * @return boolean `true` if it supports titles, `false` if not
     */
    public function hasTitleSupport()
    {
        return $this->titleSupport;
    }

    /**
     * Sets title support for slider images
     * @param boolean $supportsTitle
     */
    public function setTitleSupport($supportsTitle)
    {
        $this->titleSupport = $supportsTitle;
    }

    /**
     * Checks if Slider has subtitle support
     * @return boolean
     */
    public function hasSubtitleSupport()
    {
        return $this->subtitleSupport;
    }

    /**
     * @param boolean $supportsSubtitle
     */
    public function setSubtitleSupport($supportsSubtitle)
    {
        $this->subtitleSupport = $supportsSubtitle;
    }

    /**
     * Runs after the save method, used to save all custom_fields and other misc data to the database.
     */
    protected function saveFields()
    {
        update_post_meta($this->getId(), 'enable_title', ($this->hasTitleSupport()) ? 1 : 0);
        update_post_meta($this->getId(), 'enable_subtitle', ($this->hasSubtitleSupport()) ? 1 : 0);
    }


    /**
     * Gets an array with all of the images
     * @param $checkDate bool `false` if you want to return all images, `true` if you want to exclude images who are
     * not scheduled to be visible at the moment
     * @param $imagesAsArray bool `false` if images should be returned as objects,
     * `true` if each image should be returned as an associative array
     * @return Image[]|array[] The images (either as objects or array)
     */
    public function getImages($checkDate = false, $imagesAsArray = false) {
        $imagePosts = get_posts(
            array(
                'post_type' => Slider::SLIDER_IMAGE_POST_TYPE,
                'posts_per_page' => -1,
                'post_parent' => $this->getId(),
                'orderby' => 'menu_order',
                'order' => 'ASC'
            )
        );

        $images = array();

        foreach ($imagePosts as $imagePost) {
            $image = new Image();
            $image->loadFromPost($imagePost);

            if ($checkDate && !$image->inDateRange()) {
                continue;
            }

            if ($imagesAsArray) {
                $image = $image->asArray();
            }

            $images[] = $image;
        }

        return $images;
    }

}