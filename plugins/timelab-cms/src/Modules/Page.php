<?php
namespace Timelab\Cms\Modules;

use Timelab\Cms\Cms;
use Timelab\Cms\ModuleAbstract;
use Timelab\Cms\ApiInterface;

require_once('PageApi.php');

/**
 * This module deals with Menu and Page editing.
 * Class Page
 * @package Timelab\Cms\Modules
 */
class Page extends ModuleAbstract {

    private $api;

    const UNASSIGNED_MENU_ID = -1;
    const TRASH_MENU_ID = -2;

    public function __construct() {
        parent::__construct();
        $this->api = new PageApi();

        // Wordpress hooks
        add_action('admin_menu', array($this, 'pageEditorInit'));
    }

    /**
     * @return bool True if menu should only appear for administrators, False if it should appear for everyone
     */
    public function isAdminOnly()
    {
        return false;
    }

    /**
     * Gets a list of names of all dependant modules
     * @return string[] Array with names of all required modules
     */
    public function getDependencies()
    {
        return array('SaveHelper');
    }

    /**
     * Returns an array of javascript filenames that should be included when the module is activated.
     * @return string[] Array of javascript filenames to include
     */
    public function jsFiles()
    {
        return array("page-module.js");
    }


    /**
     * Gets the display title of the module
     * @return string
     */
    public function getMenuTitle()
    {
        return 'Undersidor';
    }

    /**
     * Gets the menu order of the module
     * @return int
     */
    public function getMenuOrder()
    {
        return 4.45454545454;
    }

    public function getMenuIcon()
    {
        return "dashicons-admin-page";
    }


    /**
     * Called on initialization of the Wordpress Admin by hooking into admin_init
     */
    public function onInitialize()
    {
        return null;
    }

    /**
     * Called when user is routed to this module, this is done during initialization of wordpress
     * (Good if you want to redirect after action is finished)
     * @param $action string|null The action to be executed
     * @param null $id int The ID of the object to perform the action on
     */
    public function earlyExecute($action = null, $id = null)
    {
        switch ($action) {
            case 'create_page':
                $this->createPage();
                break;
            case 'create_link':
                $this->createLink();
                break;
        }
    }

    /**
     * Called when user is routed to this module
     * @param $action string|null The action to be executed
     * @param null $id int The ID of the object to perform the action on
     */
    public function execute($action = null, $id = null)
    {
        switch ($action) {
            case null:
                $this->renderMenus();
                break;
            case 'save':
                $this->saveMenus();
                break;
            case 'empty_trash':
                $this->emptyTrash();
                break;
        }
    }

    /**
     * Creates a new page from the POST data.
     */
    private function createPage() {
        if (current_user_can('publish_posts')) {
            if (empty($_POST['title']) || empty($_POST['page_template'])) {
                return;
            }

            $page = wp_insert_post(array(
                'post_title'    => $_POST['title'],
                'post_type'     => 'page',
                'post_status'   => 'publish',
                'post_author'   => get_current_user_id()
            ));

            if (!empty($page)) {
                update_post_meta($page, '_wp_page_template', $_POST['page_template']);
                wp_redirect(admin_url('post.php?post=' . $page . '&action=edit'));
            }
        }
    }

    /**
     * Creates a menu link from the POST data.
     */
    private function createLink() {
        if (current_user_can('publish_posts')) {
            if (empty($_POST['title']) || empty($_POST['href']) || empty($_POST['menu']) || empty($_POST['target'])) {
                return;
            }

            $link = wp_update_nav_menu_item($_POST['menu'], 0, array(
                'menu-item-title'   => $_POST['title'],
                'menu-item-url'     => $_POST['href'],
                'menu-item-status'  => 'publish',
                'menu-item-target'  => $_POST['target']
            ));

            wp_redirect($this->getUrl());
        }
    }

    /**
     * Renders the page list
     */
    public function renderMenus() {
        $menus = $this->getMenus();

        $wpTemplates = get_page_templates();
        $templates = array();
        foreach ($wpTemplates as $name=>$template) {
            $templates[] = array('title' => $name, 'name' => $template);
        }

        $data = array(
            'menus' => $menus,
            'templates' => $templates,
            'save_url' => $this->getUrl('save'),
            'create_page_url' => $this->getUrl('create_page'),
            'create_link_url' => $this->getUrl('create_link'),
            'trash_menu_id' => self::TRASH_MENU_ID,
            'trash_url' => $this->getUrl('empty_trash')
        );

        echo $this->getCmsInstance()->render('Page/PageList.twig', $data);
    }

    /**
     * Gets the data for the page list
     * @return array The data
     */
    private function getMenus() {
        $wpMenus = wp_get_nav_menus();
        $menus = array();

        $assignedPages = array();

        // Loop trough all menus
        foreach ($wpMenus as $index=>$menu) {
            $menus[$index] = array(
                'title' => $menu->name,
                'id'    => $menu->term_id,
                'pages' => array()
            );

            $menuItems = wp_get_nav_menu_items($menu->term_id);

            // Loop trough all menu items
            foreach ($menuItems as $menuItem) {
                $assignedPages[] = $menuItem->object_id;
                if ($menuItem->menu_item_parent == 0) {
                    // Get the page
                    $page = $this->getPage($menuItem->object_id, $menuItem, $menuItems);
                    if ($page !== null) {
                        $menus[$index]['pages'][] = $page;
                    }
                }
            }
        }

        // Get all pages not assigned to menu
        $unassignedMenu = array(
            'title' => 'Ej länkade sidor',
            'id'    => self::UNASSIGNED_MENU_ID,
            'pages' => array()
        );

        $unassignedPages = get_pages(array(
            'exclude'       => $assignedPages,
            'post_status'   => 'publish,private,draft,inherit',
            'hierarchical'  => 0
        ));

        foreach ($unassignedPages as $page) {
            $unassignedMenu['pages'][] = $this->getPage($page);
        }

        $menus[] = $unassignedMenu;

        // Get all pages in the trash
        $trashMenu = array(
            'title' => 'Papperskorg',
            'id'    => self::TRASH_MENU_ID,
            'pages' => array()
        );

        $trashPages = get_pages(array(
            'exclude'       => $assignedPages,
            'post_status'   => 'trash'
        ));

        foreach ($trashPages as $page) {
            $trashMenu['pages'][] = $this->getPage($page);
        }

        $menus[] = $trashMenu;

        return $menus;
    }

    private function getPage($id, $menuItem = null, $menuItems = null) {
        // Get page associated with the link
        $page = get_post($id);

        if ($page !== null) {
            // Create page object
            $page = array(
                'title'             => ($menuItem !== null) ? $menuItem->title : $page->post_title,
                'page_title'        => $page->post_title,
                'id'                => ($menuItem !== null) ? $menuItem->ID : 0,
                'object_id'         => $page->ID,
                'menu_order'        => ($menuItem !== null) ? $menuItem->menu_order : 0,
                'parent'            => ($menuItem !== null) ? $menuItem->menu_item_parent : 0,
                'url'               => ($menuItem !== null) ? $menuItem->url : $page->guid,
                'edit_url'          => admin_url('post.php?post=' . $page->ID . '&action=edit'),
                'object_type'       => $page->post_type,
                'type'              => ($menuItem !== null) ? $menuItem->type : 'post_type',
                'target'            => ($menuItem !== null) ? $menuItem->target : '_self',
                'menu_id'           => ($menuItem !== null) ? $menuItem->menu_id : '0',
                'post_status'       => $page->post_status,
                'is_external_link'  => ($page->post_type == 'page') ? false : true,
                'children'          => array()
            );

            // Get all the children
            if ($menuItems !== null) {
                foreach ($menuItems as $child) {
                    if ($child->menu_item_parent == $menuItem->ID) {
                        $page['children'][] = $this->getPage($child->object_id, $child, $menuItems);
                    }
                }
            }

            return $page;
        }
    }

    /**
     * Saves and updates the menus
     */
    private function saveMenus() {
        /* @var $saveHelper SaveHelper */
        $saveHelper = $this->getCmsInstance()->getModule('SaveHelper');
        $data = $saveHelper->getApi()->bundlePostData($_POST);

        foreach ($data['objects'] as $menuItem) {
            // If item is not assigned to a menu, remove the link.
            if ($menuItem['menu_item_menu_id'] == self::UNASSIGNED_MENU_ID) {
                wp_delete_post($menuItem['menu_item_id']);
            }
            // If item is assigned to Trash menu, delete the link and put the page in the trash
            else if ($menuItem['menu_item_menu_id'] == self::TRASH_MENU_ID) {
                wp_delete_post($menuItem['menu_item_id']);
                wp_trash_post($menuItem['menu_item_object_id']);
            }

            // If item is not assigned to trash, untrash
            if ($menuItem['menu_item_menu_id'] != self::TRASH_MENU_ID) {
                wp_untrash_post($menuItem['menu_item_object_id']);
            }
            
            // Update the menu item
            wp_update_nav_menu_item($menuItem['menu_item_menu_id'], $menuItem['menu_item_id'],
                array(
                    "menu_item_db_id" =>        $menuItem['menu_item_id'],
                    "menu-item-object-id" =>    $menuItem['menu_item_object_id'],
                    "menu-item-parent-id" =>    $menuItem['menu_item_parent'],
                    "menu-item-position" =>     $menuItem['menu_item_order'],
                    "menu-item-type" =>         $menuItem['menu_item_type'],
                    "menu-item-title" =>        $menuItem['menu_item_title'],
                    "menu-item-url" =>          $menuItem['menu_item_url'],
                    "menu-item-status" =>       'publish',
                    "menu-item-object" =>       $menuItem['menu_item_object']
                )
            );

            update_post_meta($menuItem['menu_item_id'], 'target', $menuItem['menu_item_target']);
        }

        $this->execute();
    }

    /**
     * Empties the trash
     */
    private function emptyTrash() {
        $trashPages = get_pages(array(
            'post_status'   => 'trash'
        ));

        foreach($trashPages as $page) {
            wp_delete_post($page->ID, true);
        }
    }

    public function pageEditorInit() {
        // If not admin, remove a bunch of metaboxes
        if (!Cms::isSuperAdmin()) {
            remove_meta_box('pageparentdiv', 'page', 'normal');
            remove_meta_box('authordiv', 'page', 'normal');
            remove_meta_box('slugdiv', 'page', 'normal');
            remove_meta_box('commentsdiv', 'page', 'normal');
            remove_meta_box('commentstatusdiv', 'page', 'normal');
            remove_meta_box('postcustom', 'page', 'normal');

            add_meta_box('timelab_submitdiv', 'Publicera', array($this, 'renderPublishBox'), 'page', 'side', 'high');
        }
    }

    public function renderPublishBox() {
        $data = array(
            'id'        => get_the_ID(),
            'site_url'  => get_site_url(),
            'trash_url' => $this->getUrl('trash', get_the_ID()),
            'templates' => array()
        );

        foreach (get_page_templates() as $pageTemplateName=>$pageTemplate) {
            $data['templates'][] = array(
                'name'      => $pageTemplateName,
                'file'      => $pageTemplate,
                'selected'  => (get_post_meta($data['id'], '_wp_page_template', true) == $pageTemplate) ? true : false
            );
        }

        echo $this->getCmsInstance()->render('Page/PublishBox.twig', $data);
    }

    /**
     * Returns the API interface of the module
     * @return ApiInterface
     */
    public function getApi()
    {
        return $this->api;
    }

}