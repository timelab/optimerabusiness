<?php
/**
 * Created by PhpStorm.
 * User: Alexander
 * Date: 2014-08-11
 * Time: 10:26
 */

namespace Timelab\Cms\Modules;


use Timelab\Cms\ApiAbstract;

class SaveHelperApi extends ApiAbstract {

    /**
     * Bundles all arrays in POST data into objects.
     * if set to accept multiple types, it will get object name from everything before the last underscore, while the
     * field will get it's name from the last segment. For instance `admin_user_isCool` will become `admin_user['isCool']`
     * @param $data array The data to bundle
     * @param $multipleTypes bool `true` if there is more than one object type, `false` if not.
     * @throws \Exception If data is invalid
     * @return array The bundled data
     */
    public function bundlePostData($data, $multipleTypes = false) {
        // Build the objects
        $objects = array();

        foreach ($data as $keyName=>$key) {
            if (is_array($key)) {
                if ($multipleTypes) {
                    $name = explode('_', $keyName);
                    $fieldName = array_pop($name);
                    $objectName = implode('_', $name);
                    for ($i = 0; $i < count($key); $i++) {
                        $objects[$objectName][$i][$fieldName] = $key[$i];
                    }
                } else {
                    for ($i = 0; $i < count($key); $i++) {
                        $objects[$i][$keyName] = $key[$i];
                    }
                }
            }
        }

        $returnData = array(
            "objects" => $objects
        );

        // Add all non-array values to the data
        foreach ($data as $keyName=>$key) {
            if (!is_array($key)) {
                $returnData[$keyName] = $key;
            }
        }

        return $returnData;
    }
} 