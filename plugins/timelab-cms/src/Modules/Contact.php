<?php

namespace Timelab\Cms\Modules;


use Timelab\Cms\ApiInterface;
use Timelab\Cms\ModuleAbstract;
use Timelab\Cms\Objects\Address;
use Timelab\Cms\Objects\ContactDetail;
use Timelab\Cms\Objects\ContactDetailType;
use Timelab\Cms\Objects\Facility;
use Timelab\Cms\Objects\Image;
use Timelab\Cms\Objects\Person;
use Timelab\Cms\Objects\SettingType;
use Timelab\Cms\Objects\TimeSpan;
use Timelab\Cms\Objects\SocialDetail;
use Timelab\Cms\Objects\SocialDetailType;

require_once('ContactApi.php');
require_once(__DIR__.'/../Objects/Facility.php');
require_once(__DIR__.'/../Objects/Address.php');
require_once(__DIR__.'/../Objects/ContactDetail.php');
require_once(__DIR__.'/../Objects/TimeSpan.php');
require_once(__DIR__.'/../Objects/Person.php');
require_once(__DIR__.'/../Objects/Image.php');
require_once(__DIR__.'/../Objects/SocialDetail.php');

/**
 * Enables the contact information administration, this enables users to administrate Personnel, Contact Adress and
 * opening hours, optionally on a facility level
 * 
 * Class Contact
 * @package Timelab\Cms\Modules
 */
class Contact extends ModuleAbstract {

    private $api;

    function __construct()
    {
        parent::__construct();
        $this->api = new ContactApi();
    }


    public function getMenuTitle()
    {
        return 'Kontaktuppgifter';
    }


    /**
     * @return bool True if menu should only appear for administrators, False if it should appear for everyone
     */
    public function isAdminOnly()
    {
        return false;
    }

    /**
     * Returns an array of javascript filenames that should be included when the module is activated.
     * @return string[] Array of javascript filenames to include
     */
    public function jsFiles()
    {
        return array('contact-module.js');
    }

    /**
     * Called on initialization of the Wordpress Admin by hooking into admin_init
     */
    public function onInitialize()
    {
        $this->registerSetting(
            'social',
            'Enable Social',
            'Allows the user to administrate and update the Social links',
            SettingType::BOOL
        );

        $this->registerSetting(
            'description',
            'Enable Description',
            'Allows the user to administrate and update the descriptions of their facilities',
            SettingType::BOOL
        );

        $this->registerSetting(
            'address',
            'Enable Addresses',
            'Allows the user to update the address of their facilities',
            SettingType::BOOL
        );

        $this->registerSetting(
            'info',
            'Enable Contact Information',
            'Allows the user to administrate their contact information (E-mail, phone, fax etc)',
            SettingType::BOOL
        );

        $this->registerSetting(
            'hours',
            'Enable Opening Hours',
            'Allows the user to update and administrate the opening hours of their facilities',
            SettingType::BOOL
        );

        $this->registerSetting(
            'personnel',
            'Enable Personnel',
            'Allows the user to administrate and update the personnel of their facilities',
            SettingType::BOOL
        );
    }

    /**
     * Gets a list of names of all dependant modules
     * @return string[]|null Array with names of all required modules, null if no dependencies
     */
    public function getDependencies()
    {
        return array('SaveHelper');
    }

    /**
     * Gets the menu order of the module
     * @return int
     */
    public function getMenuOrder()
    {
        return 1001;
    }

    public function getMenuIcon()
    {
        return "dashicons-email-alt";
    }


    /**
     * Called when user is routed to this module, this is done during initialization of wordpress
     * (Good if you want to redirect after action is finished)
     * @param $action string|null The action to be executed
     * @param null $id int The ID of the object to perform the action on
     */
    public function earlyExecute($action = null, $id = null)
    {
        switch ($action) {
            case 'create':
                $this->createFacility();
                break;
            case 'create_translation':
                $this->createTranslation($id);
                break;
            case 'save_facilities':
                $this->saveFacilities();
                break;
            case 'save':
                $this->saveFacility($id);
                break;
            case 'delete':
                $this->deleteFacility($id);
                break;
        }
    }

    /**
     * Called when user is routed to this module
     * @param $action string|null The action to be executed
     * @param null $id int The ID of the object to perform the action on
     */
    public function execute($action = null, $id = null)
    {
        switch ($action) {
            case 'edit':
                $this->renderEditor($id);
                break;
            default:
                $this->renderList();
                break;
        }
    }

    private function renderList() {
        $facilities = $this->getApi()->getFacilities(true);

        foreach ($facilities as $index=>$facility) {
            $facility['edit_url'] = $this->getUrl('edit', $facility['id']);
            $facilities[$index] = $facility;
        }

        $data = array(
            'facilities'    => $facilities,
            'create_url'    => $this->getUrl('create'),
            'save_url'      => $this->getUrl('save_facilities')
        );

        echo $this->getCmsInstance()->render('Contact/FacilityList.twig', $data);
    }

    private function renderEditor($id) {
        $facility = $this->getApi()->getFacility($id, true);
        $languages = array();
        $facilityLanguage = array();
        if ($this->getCmsInstance()->getModule('Wpml') !== null) {
            /** @var $wpmlApi WpmlApi */
            $wpmlApi = $this->getCmsInstance()->getApi('Wpml');
            $f = new Facility();
            $languages = $wpmlApi->getTranslationsOfPost($id, $f->getPostType());
            $facilityLanguage = $wpmlApi->getPostLanguage($id);
        }

        // Get tinyMCE editor for description
        ob_start(); // Start output buffering
        wp_editor($facility['description'], 'description_editor');
        $editor = ob_get_contents(); // Store buffer in variable
        ob_end_clean(); // End buffering and clean up

        $data = array (
            'facility'              => $facility,
            'save_url'              => $this->getUrl('save', $id),
            'delete_url'            => $this->getUrl('delete', $id),
            'contact_detail_types'  => ContactDetailType::getTypes(),
            'social_detail_types'   => SocialDetailType::getTypes(),
            'description_enabled'   => $this->getSetting('description')->getValue(),
            'addresses_enabled'     => $this->getSetting('address')->getValue(),
            'contact_info_enabled'  => $this->getSetting('info')->getValue(),
            'social_info_enabled'   => $this->getSetting('social')->getValue(),
            'opening_hours_enabled' => $this->getSetting('hours')->getValue(),
            'personnel_enabled'     => $this->getSetting('personnel')->getValue(),
            'languages'             => $languages,
            'facility_language'     => $facilityLanguage,
            'translate_url'         => $this->getUrl('create_translation', $id),
            'edit_url'              => $this->getUrl('edit'),
            'description_editor'    => $editor
        );

        echo $this->getCmsInstance()->render('Contact/ContactEditor.twig', $data);
    }

    private function createFacility() {

        if (isset($_POST['title'])) {
            $facility = new Facility();
            $facility->setTitle($_POST['title']);
            $facility->setOrder(100);
            $facility->save();

            wp_redirect($this->getUrl('edit', $facility->getId()));
            return;
        }

        wp_redirect($this->getUrl());
    }

    private function saveFacilities() {
        /** @var $saveHelper SaveHelperApi */
        $saveHelper = $this->getCmsInstance()->getApi('SaveHelper');
        $data = $saveHelper->bundlePostData($_POST);

        foreach ($data['objects'] as $facilityData) {
            $facility = new Facility();
            $facility->loadFromPost(get_post($facilityData['id']));

            $facility->setTitle($facilityData['title']);
            $facility->setOrder($facilityData['order']);

            $facility->save();
        }

        wp_redirect($this->getUrl());
    }

    private function saveFacility($id) {
        /** @var $saveHelper SaveHelperApi */
        $saveHelper = $this->getCmsInstance()->getApi('SaveHelper');
        $data = $saveHelper->bundlePostData($_POST, true);

        $facility = new Facility();
        $facility->loadFromPost(get_post($id));

        
        $facility->setDescription($data['description']);            
        $facility->save();

        // Save address
        if (isset($data['objects']['address'])) {
            foreach ($data['objects']['address'] as $addressData) {
                $address = new Address();

                if (!empty($addressData['id'])) {
                    $address->loadFromPost(get_post($addressData['id']));
                }

                $address->setTitle($addressData['formatted']);
                $address->setFormatted($addressData['formatted']);
                $address->setStreet($addressData['street']);
                $address->setZip($addressData['zip']);
                $address->setCity($addressData['city']);
                $address->setLat($addressData['lat']);
                $address->setLong($addressData['long']);
                $address->setParentId($id);

                $address->save();
            }
        }

        // Save contact Details
        if (isset($data['objects']['contact_details'])) {
            foreach ($data['objects']['contact_details'] as $contactData) {
                $this->saveContactDetail($id, $contactData);
            }
        }

        // Save Social Details
        if (isset($data['objects']['social_details'])) {
            foreach ($data['objects']['social_details'] as $socialData) {
                $this->saveSocialDetail($id, $socialData);
            }
        }


        // Save opening hours
        if (isset($data['objects']['opening_hours'])) {
            foreach ($data['objects']['opening_hours'] as $openingHoursData) {
                $openingHours = new TimeSpan();

                if (!empty($openingHoursData['id'])) {
                    $openingHours->loadFromPost(get_post($openingHoursData['id']));
                }

                if (!empty($openingHoursData['remove'])) {
                    if ($openingHours->getId() !== null) {
                        $openingHours->trash();
                    } else {
                        continue;
                    }
                }

                $openingHours->setParentId($id);
                $openingHours->setTitle($openingHoursData['title']);
                $openingHours->setFrom($openingHoursData['from']);
                $openingHours->setTo($openingHoursData['to']);
                $openingHours->setDepartment($openingHoursData['department']);
                $openingHours->setOrder($openingHoursData['order']);

                $openingHours->save();
            }
        }

        // Save personnel
        if (isset($data['objects']['person'])) {
            foreach ($data['objects']['person'] as $index=>$personData) {
                $person = new Person();

                if (!empty($personData['id'])) {
                    $person->loadFromPost(get_post($personData['id']));
                }

                if (!empty($personData['remove'])) {
                    if ($person->getId() !== null) {
                        $person->trash();
                    } else {
                        continue;
                    }
                }

                $person->setParentId($id);
                $person->setOrder($personData['order']);
                $person->setName($personData['name']);
                $person->setTitle($person->getName());
                $person->setRole($personData['role']);
                $person->setDepartment($personData['department']);
                $person->setDescription($personData['description']);
                $person->save();

                // Save person contact details
                for ($i = 0; $i < 3; $i++) {
                    if (!empty($data['objects']['personnel_contact_details'])) {
                        $contactDetailData = array_shift($data['objects']['personnel_contact_details']);
                        $this->saveContactDetail($person->getId(), $contactDetailData);
                    }
                }

                // Person image
                $image = new Image();

                if (isset($data['objects']['person_image'][$index])) {
                    $imageData = $data['objects']['person_image'][$index];

                    if (!empty($imageData['id'])) {
                        $image->loadFromPost(get_post($imageData['id']));
                    }

                    $image->setMediaId($imageData['mediaId']);
                    $image->setParentId($person->getId());
                    $image->save();
                }

            }
        }

        //wp_redirect($this->getUrl('edit', $id));
    }

    private function deleteFacility($id) {
        $facility = new Facility();
        $facility->loadFromPost(get_post($id));

        $facility->trash();

        wp_redirect($this->getUrl());
    }

    private function createTranslation($id) {

        if ($this->getCmsInstance()->getModule('Wpml') !== null && isset($_GET['lang_code'])) {
            /** @var $wpmlApi WpmlApi */
            $wpmlApi = $this->getCmsInstance()->getModule('Wpml')->getApi();
            $facility = new Facility();
            $tid = $wpmlApi->translatePost($id, $facility->getPostType(), $_GET['lang_code']);
            wp_redirect($this->getUrl('edit', $tid));

            return;
        }

        wp_redirect($this->getUrl());
    }

    /**
     * Returns the API interface of the module
     * @return ContactApi
     */
    public function getApi()
    {
        return $this->api;
    }

    /**
     * @param $facilityId
     * @param $data
     * @throws \Exception
     */
    private function saveContactDetail($facilityId, $data)
    {
        $contactDetail = new ContactDetail();
        if (!empty($data['id'])) {
            $contactDetail->loadFromPost(get_post($data['id']));
        }

        $contactDetail->setTitle($data['value']);
        $contactDetail->setValue($data['value']);
        $contactDetail->setType($data['type']);
        $contactDetail->setParentId($facilityId);

        $contactDetail->save();
    }

    private function saveSocialDetail($facilityId, $data)
    {
        $socialDetail = new SocialDetail();
        if (!empty($data['id'])) {
            $socialDetail->loadFromPost(get_post($data['id']));
        }

        $socialDetail->setTitle($data['value']);
        $socialDetail->setValue($data['value']);
        $socialDetail->setType($data['type']);
        $socialDetail->setParentId($facilityId);

        $socialDetail->save();
    }
}