<?php

namespace Timelab\Cms\Modules;


use Timelab\Cms\ApiAbstract;
use Timelab\Cms\Cms;
use Timelab\Cms\Objects\SliderObject;

require_once(__DIR__.'/../Objects/SliderObject.php');

class SliderApi extends ApiAbstract {

    /**
     * Gets and returns all the sliders
     *
     * @example `$cms->getModule('Slider')->getSliders();`
     *
     * @return SliderObject[] Array containing all the sliders
     */
    public function getSliders() {
        $sliderPosts = get_posts(
            array(
                'post_type'     => Slider::SLIDER_POST_TYPE,
                'post_status'   => 'publish',
                'posts_per_page'    => -1
            )
        );

        $sliders = array();

        foreach ($sliderPosts as $sliderPost) {
            $slider = new SliderObject();
            $slider->loadFromPost($sliderPost);
            $sliders[] = $slider;
        }

        return $sliders;
    }

    /**
     * Gets and returns the specified slider.
     * @param $id int The ID of the slider.
     * @param $ignoreTranslations bool Always fetch the specified slider, even if translation is available.
     * @return null|SliderObject Returns the specified slider, `null` if the slider could not be found.
     */
    public function getSlider($id, $ignoreTranslations = false) {

        $slider = new SliderObject();

        if ($ignoreTranslations == false && Cms::$instance->getModule('Wpml') !== null) {
            $translations = Cms::$instance->getApi('Wpml')->getTranslationsOfPost($id, $slider->getPostType());

            foreach ($translations as $translation) {
                if ($translation['language_code'] === ICL_LANGUAGE_CODE) {
                    $slider->loadFromPost(get_post($translation['post_id']));
                    return $slider;
                }
            }
        }


        $slider->loadFromPost(get_post($id));
        return $slider;
    }
}