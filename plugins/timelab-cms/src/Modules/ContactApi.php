<?php
namespace Timelab\Cms\Modules;

use Timelab\Cms\ApiAbstract;
use Timelab\Cms\Objects\Facility;

class ContactApi extends ApiAbstract {

    /**
     * Gets the specified facility from the database.
     * @param $id int The ID of the facility you want to get.
     * @param $asArray bool `true` if you want to return facility as an associative array instead of an object
     * @return array|Facility
     * @throws \Exception
     */
    public function getFacility($id, $asArray = false) {
        $facility = new Facility();
        $facility->loadFromPost(get_post($id));

        if ($asArray) {
            $facility = $facility->asArray();
        }

        return $facility;
    }

    /**
     * Gets all published facilities from the database.
     * @param $asArray bool `true` if you want to return facilities as an associative array instead of as objects
     * @return Facility[]|array[] Array of Facility objects if $asArray is `false`, if `true` returns array of arrays
     * @throws \Exception
     */
    public function getFacilities($asArray = false) {
        $facility = new Facility();

        $facilityPosts = get_posts(
            array(
                'post_type' => $facility->getPostType(),
                'post_status' => 'publish',
                'orderby' => 'menu_order',
                'order' => 'ASC',
                'posts_per_page' => -1
            )
        );

        $facilities = array();

        foreach ($facilityPosts as $facilityPost) {
            $facility = new Facility();
            $facility->loadFromPost($facilityPost);

            if ($asArray) {
                $facility = $facility->asArray();
            }

            $facilities[] = $facility;
        }

        return $facilities;
    }
} 